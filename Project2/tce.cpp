//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

// get current file (dirname/prog) name base and dirname
// check if dirname/__prog.bat exists
// run dirname/__prog.bat with current arguments

#include <cstring>
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <windows.h>

using namespace std;

struct PathSplit
{
   PathSplit(const std::string & name)
   {
      const auto last_slash = find_last_slash(name);
      if(last_slash == std::string::npos)
      {
         dirname.clear();
         basename = name;
         return;
      }

      dirname = name.substr(0, last_slash + 1);
      basename = name.substr(last_slash + 1);
      // cout << "dirn " << dirname << " base " << basename << endl;
   }

   static size_t find_last_slash(const std::string & name)
   {
      const auto xpos = std::string::npos;
      auto pos1 = name.find_last_of('\\');
      auto pos2 = name.find_last_of('/');
      if(pos1 != xpos && pos2 != xpos)
      {
         return max(pos1, pos2);
      }

      return pos1 != xpos ? pos1 : pos2;
   }

   // get the .bat corresponding file
   std::string get_bat() const
   {
      // determine if ends in .exe
      auto basec = basename;
      if (basename.size() > 4)
      {
         auto last4 = basename.substr(basename.size() - 4);
         std::transform(last4.begin(), last4.end(), last4.begin(), ::tolower);
         if(last4 == ".exe")
         {
            basec = basec.substr(0, basec.size() - 4);
         }

      }

      std::string out = dirname + "\\__" + basec + ".bat";
      return out;
   }

   std::string dirname;
   std::string basename;
};



string quoteIfNonOption(const string & name) {
	if (name[0] == '-' || name[0] == '/' || name[0] == '"')
		return name;
	return "\"" + name + "\"";
}

void runCmd(const string & cmd) {
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// Start the child process.
	const size_t c_cmd_size = cmd.length() + 1;
	char * c_cmd = new char[c_cmd_size];
	strncpy_s(c_cmd, c_cmd_size, cmd.c_str(), c_cmd_size);
	// std::cout << std::string(c_cmd) << std::endl;
	if (!CreateProcess(NULL,   // No module name (use command line)
		c_cmd,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		std::cerr << "CreateProcess failed " << GetLastError() << std::endl;
		exit(1);
	}

	// Wait until child process exits.
	//WaitForSingleObject( pi.hProcess, INFINITE );

	// Close process and thread handles.
	//CloseHandle( pi.hProcess );
	//CloseHandle( pi.hThread );
}

int main(const int argc, const char ** argv)
{
   // get current program name
   //wchar_t path[MAX_PATH] = {0};
   //GetModuleFileNameW(NULL, path, MAX_PATH);
   char path[MAX_PATH] = {0};
   GetModuleFileNameA(NULL, path, MAX_PATH);

   // cout << "path = " << path << endl;
   auto ps = PathSplit(std::string(path));
   // cout << ps.get_bat() << endl;

   // get other options passed in command line
   vector<string> otherOptions;
   for (int i = 1; i < argc; i++)
      otherOptions.push_back(argv[i]);

   string thisCmd = ps.get_bat();
   for (auto opt : otherOptions)
      //thisCmd += " " + quoteIfNonOption(opt);
      thisCmd += " " + (opt);
   runCmd(thisCmd);
   return 0;
}