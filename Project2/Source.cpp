#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <windows.h>

using namespace std;

vector<string> getFileNamesFromFile(const string & name)
{
	vector<string> output;
	ifstream infile(name);
	string line;
	while (getline(infile, line))
		output.push_back(line);
	return output;
}

string quoteIfNonOption(const string & name) {
	if (name[0] == '-' || name[0] == '/' || name[0] == '"')
		return name;
	return "\"" + name + "\"";
}

void runCmd(const string & cmd) {
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// Start the child process.
	const size_t c_cmd_size = cmd.length() + 1;
	char * c_cmd = new char[c_cmd_size];
	strncpy_s(c_cmd, c_cmd_size, cmd.c_str(), c_cmd_size);
	std::cout << std::string(c_cmd) << std::endl;
	if (!CreateProcess(NULL,   // No module name (use command line)
		c_cmd,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		)
	{
		std::cerr << "CreateProcess failed " << GetLastError() << std::endl;
		exit(1);
	}

	// Wait until child process exits.
	//WaitForSingleObject( pi.hProcess, INFINITE );

	// Close process and thread handles.
	//CloseHandle( pi.hProcess );
	//CloseHandle( pi.hThread );
}

int main(const int argc, const char ** argv)
{
	if (argc < 3) {
		cerr << "Please provide 2 arguments to program" << endl;
		exit(1);
	}

	const string cmd = quoteIfNonOption(argv[1]);
	const string fileName = argv[argc - 1];
	vector<string> otherOptions;
	for (int i = 2; i < argc - 1; i++)
		otherOptions.push_back(argv[i]);

	const auto eachIt = find(otherOptions.begin(), otherOptions.end(), "-each");
	const bool hasEachFlag = eachIt != otherOptions.end();
	const vector<string> filesFromFile = getFileNamesFromFile(fileName);
	if (hasEachFlag) {
		otherOptions.erase(eachIt);
		for (auto fname : filesFromFile) {
			string thisCmd = cmd;
			for (auto opt : otherOptions)
				thisCmd += " " + quoteIfNonOption(opt);
			thisCmd += " " + quoteIfNonOption(fname);
			runCmd(thisCmd);
		}
	}
	else {
		string thisCmd = cmd;
		for (auto opt : otherOptions)
			thisCmd += " " + quoteIfNonOption(opt);
		for (auto fname : filesFromFile)
			thisCmd += " " + quoteIfNonOption(fname);
		runCmd(thisCmd);
	}
	return 0;
}